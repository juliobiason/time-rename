#!/usr/bin/python

import os
import os.path
import sys
import glob
import optparse
import datetime

commands = optparse.OptionParser()
commands.add_option('-f', '--format',
        help='Rename format',
        dest='mask',
        default='%Y%m%d-%I%M%S')

(options, args) = commands.parse_args()

for mask in args:
    files = glob.glob(mask)
    for file in files:
        statinfo = os.stat(file)
        created = datetime.datetime.fromtimestamp(statinfo.st_ctime)
        base = os.path.dirname(file)
        (_, ext) = os.path.splitext(file)
        dest = os.path.join(base, created.strftime(options.mask) + ext)
        os.rename(file, dest)
